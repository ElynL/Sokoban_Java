#include "personnage.h"

Personnage::Personnage(int x, int y):Mobile(x,y){}

Personnage::Personnage(const Personnage &p):Mobile(p){}

void Personnage::dessinerObjetGraphique(QPainter*p, Coordonnees pos, int t){
    Coordonnees c=getCoordonnees();
    p->drawPixmap(pos.y+c.y*t,pos.x+c.x*t,t,t,QPixmap(":/images/pingouin.png"));
}
