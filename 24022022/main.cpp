#include "mainwindow.h"
#include "menu.h"
#include "aide.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow main;
    Aide ai;
    Menu menu(&ai,&main);
    menu.show();
    return a.exec();
}
