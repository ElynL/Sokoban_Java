#include "mobile.h"

Mobile::Mobile(int x,int y):ObjetGraphique(x,y){
}

Mobile::Mobile(const Mobile &m):ObjetGraphique(m){
}

void Mobile::updateCoordonnees(Coordonnees c){
    this->c.x+=c.x;
    this->c.y+=c.y;
}

Mobile::~Mobile(){}
