/**
  * @file sol.h
  * @brief Contient la déclaration de la classe Sol
  * @details La classe \c Sol hérite de la classe \c Fixe
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef SOL_H
#define SOL_H

#include "fixe.h"
class Sol:public Fixe
{
public:
    /**
     * @brief Sol
     * @param x : coordonnées en abscisse du sol
     * @param y : coordonnées en ordonnées du sol
     * @details : initialise les coordonnées du sol à la position (x,y)
     */
    Sol(int x=0, int y=0,bool cible=false);
    /**
     * @brief Sol
     * @param x : coordonnées en abscisse du sol
     * @param y : coordonnées en ordonnées du sol
     * @details : initialise les coordonnées du sol à la position (x,y)
     */
    Sol(const Sol &s);
    /**
     * @brief dessinerObjetGraphique
     * @param p : pointeur de QPainter
     * @param pos : Coordonnées du point en haut à gauche du plateau
     * @param t : entier correspondant à la taille en pixels du sol à l'écran
     * @details : permet l'affichage du sol à l'écran
     */
    void dessinerObjetGraphique(QPainter*p,Coordonnees pos, int t);
    /**
     * @brief getCible
     * @return 2
     */
    bool getCible() const;
    /**
     * @brief getType
     * @return vrai si c'est une cible, faux sinon
     */
    int getType() const;
private:
    bool cible;
};

#endif // SOL_H
