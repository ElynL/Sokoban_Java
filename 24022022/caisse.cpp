#include "caisse.h"

Caisse::Caisse(int x, int y):Mobile(x,y){}

Caisse::Caisse(const Caisse &c):Mobile(c){}

void Caisse::dessinerObjetGraphique(QPainter*p,Coordonnees pos, int t, int i){
    Coordonnees c=getCoordonnees();
    if (i==0){
        p->drawPixmap(pos.y+c.y*t,pos.x+c.x*t,t,t,QPixmap(":/images/caisse.jpg"));
    }
    else {
        p->drawPixmap(pos.y+c.y*t,pos.x+c.x*t,t,t,QPixmap(":/images/caisseB.png"));
    }
}

void Caisse::dessinerObjetGraphique(QPainter*p,Coordonnees pos, int t){
}
