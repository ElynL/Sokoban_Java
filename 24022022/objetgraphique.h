/**
  * @file objetgraphique.h
  * @brief Contient la déclaration de la classe ObjetGraphique
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef OBJETGRAPHIQUE_H
#define OBJETGRAPHIQUE_H

#include <QPainter>
using namespace std;

/**
 * @struct Coordonnées
 * @brief La structure Coordonnées possède deux attributs entiers x et y
*/
struct Coordonnees{
    int x;
    int y;
};

/**
 * @def lien
 * @brief Définit le chemin pour récupérer les fichiers textes niveau
 */
const string lien="/home/polytech/Documents/3A/24022022";

class ObjetGraphique{
    public:
        /**
         * @brief ObjetGraphique
         * @param x : Coordonnée en abscisses
         * @param y : Coordonnée en ordonnée
         * @details : permet d'initialiser les coordonnées de l'objet graphique à (x,y)
         */
        ObjetGraphique(int x=0, int y=0);
        /**
         * @brief ObjetGraphique
         * @param o : référence d'objet graphique
         * @details : permet de recopier les informations d'un objet graphique
         */
        ObjetGraphique(const ObjetGraphique &o);
        /**
         * @brief getCoordonnees
         * @return les coordonnées de l'objet graphique
         */
        Coordonnees getCoordonnees() const;
        /**
         * @brief initialiser
         * @param nC : Coordonnées
         * @details : permet d'initialiser les coordonnées de l'objet graphique à nC
         */
        void initialiser(Coordonnees nC);
        // Méthodes virtuelles
        virtual void dessinerObjetGraphique(QPainter *p, Coordonnees pos, int t)=0;
        virtual ~ObjetGraphique();

    protected:
        Coordonnees c;
};

#endif // OBJETGRAPHIQUE_H
