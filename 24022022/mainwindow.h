/**
  * @file mainwindow.h
  * @brief Contient la déclaration de la fenêtre principale
  * @details La fenêtre mainwindow permet l'affichage du plateau
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QPainter>
#include <personnage.h>
#include <QKeyEvent>
#include <caisse.h>
#include <personnage.h>
#include <mur.h>
#include <sol.h>
#include <niveau.h>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief MainWindow
     * @param parent : pointeur de QWidget
     */
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    /**
     * @brief changeNiveau
     * @param nom : chaine de caractère
     * @details : permet de changer le niveau
     */
    void changeNiveau(string nom);

private slots:
    /**
     * @brief on_BRenitialiser_clicked
     * @details : permet après clic de l'utilisateur de rénitialiser le niveau
     */
    void on_BRenitialiser_clicked();
    /**
     * @brief on_BQuitter_clicked
     * @details : permet après clic de l'utilisateur de quitter le jeu
     */
    void on_BQuitter_clicked();

private:
    Ui::MainWindow *ui;
    Niveau *g;
    /**
     * @brief paintEvent
     * @param e : pointeur de QPaintEvent
     * @details : permet d'afficher la fenêtre à l'écran
     */
    void paintEvent(QPaintEvent *e);
    /**
     * @brief keyPressEvent
     * @param event : pointeur de QKeyEvent
     * @details : Permet de détecter les touches utiliser par l'utilsateur nb
     */
    void keyPressEvent(QKeyEvent * event);
};
#endif // MAINWINDOW_H
