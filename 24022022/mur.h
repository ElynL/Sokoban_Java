/**
  * @file mur.h
  * @brief Contient la déclaration de la classe Mur
  * @details La classe \c Mur hérite de la classe \c Fixe
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef MUR_H
#define MUR_H

#include <fixe.h>

class Mur:public Fixe{
    public:
        /**
         * @brief Mur
         * @param x : Coordonnée en abscisse du mur
         * @param y : Coordonnée en ordonnée du mur
         * @details : permet d'initialiser les coordonnées du Mur
         */
        Mur(int x=0, int y=0);
        /**
         * @brief Mur
         * @param m : référence de mur
         * @details : permet de recopier les informations d'un mur
         */
        Mur(const Mur &m);
        /**
         * @brief dessinerObjetGraphique
         * @param p : pointeur de QPainter
         * @param pos : Coordonnées du point en haut à gauche du plateau
         * @param t : entier correspondant à la taille en pixels du mur à l'écran
         * @details : permet l'affichage du mur à l'écran
         */
        void dessinerObjetGraphique(QPainter*p,Coordonnees pos,int t);
        /**
         * @brief getType
         * @return 1
         */
        int getType() const;
};

#endif // MUR_H
