#include "menu.h"
#include "ui_menu.h"
#include <QScreen>
#include <QString>
#include "string.h"

Menu::Menu(Aide *a, MainWindow *m, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Menu)
{
    ui->setupUi(this);
    QPixmap bkgnd(":/images/fond.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    this->setWindowState(Qt::WindowMaximized);
    niveau = m;
    aide = a;
}

Menu::~Menu()
{
    delete ui;
}

void Menu::on_BJouer_clicked()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    const int screenWidth  = screen->size().width();
    const int screenHeight = screen->size().height();
    niveau->move((screenWidth-800)/2,(screenHeight-800)/2);
    niveau->show();
    this->close();
}

void Menu::on_BQuitter_clicked()
{
    this->close();
}

void Menu::on_BAide_clicked()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    const int screenWidth  = screen->size().width();
    const int screenHeight = screen->size().height();
    aide->move((screenWidth-550)/2,(screenHeight-455)/2);
    aide->show();
}

void Menu::on_comboBox_activated(const QString &arg1)
{
    string s=lien+"/"+arg1.toStdString();
    s=s+".txt";

    niveau->changeNiveau(s);
}

void Menu::on_BFin_clicked()
{
    this->close();
}
