/**
  * @file mobile.h
  * @brief Contient la déclaration de la classe Mobile
  * @details La classe \c Mobile hérite de la classe \c Objet \c graphique
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef MOBILE_H
#define MOBILE_H

#include <objetgraphique.h>

class Mobile: public ObjetGraphique {
    public:
        /**
         * @brief Mobile
         * @param x : Coordonées en abscices
         * @param y : Coordonées en ordonnée
         * @details : permet d'initialiser la position de l'objet graphique Mobile
         */
        Mobile(int x=0, int y=0);
        /**
         * @brief Mobile
         * @param m : référence de Mobile
         * @details : permet de recopier les informations de mobile
         */
        Mobile(const Mobile &m);
        // Méthode virtuelle
        virtual ~Mobile();
        /**
         * @brief updateCoordonnees
         * @param c : Coordonnées
         * @details : permet d'ajouter aux coordonnées de Mobile c
         */
        void updateCoordonnees(Coordonnees c);

};

#endif // MOBILE_H
