/**
  * @file caisse.h
  * @brief Contient la déclaration de la classe Caisse
  * @details La classe \c Caisse hérite de la classe \c Mobile
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/

#ifndef CAISSE_H
#define CAISSE_H
#include "mobile.h"

class Caisse:public Mobile
{
public:
    /**
     * @brief Caisse
     * @param x : coordonnées en abscisse de la caisse
     * @param y : coordonnées en ordonnées de la caisse
     * @details : initialise les coordonnées de la caisse à la position (x,y)
     */
    Caisse(int x=0, int y=0);
    /**
     * @brief Caisse
     * @param c : référence de caisse
     * @details : Permet de recopier les informations d'une caisse dans une autre caisse
     */
    Caisse(const Caisse &c);
    /**
     * @brief dessinerObjetGraphique
     * @param p : pointeur de QPainter
     * @param pos : Coordonnées du point en haut à gauche du plateau
     * @param t : entier correspondant à la taille en pixels de la caisse à l'écran
     * @details : permet l'affichage de la caisse à l'écran
     */
    void dessinerObjetGraphique(QPainter*p, Coordonnees pos, int t);
    // On surcharge la méthode dessinerObjetGraphique
    // l'entier i permet de savoir s'il faut afficher une caisse marron ou bleue
    // Si i vaut 0, on affiche la caisse marron, sinon on affiche la caisse bleue
    /**
     * @brief dessinerObjetGraphique
     * @param p : pointeur de QPainter
     * @param pos : Coordonnées du point en haut à gauche du plateau
     * @param t : entier correspondant à la taille en pixels de la caisse à l'écran
     * @param i : entier permettant de déterminer la couleur de la caisse à afficher
     * @details : surcharge de la méthode / permet l'affichage de la caisse à l'écran
     * @details : Si i vaut 0, on affiche la caisse marron, sinon on affiche la caisse bleue
     */
    void dessinerObjetGraphique(QPainter*p,Coordonnees pos,int t, int i);
};

#endif // CAISSE_H
