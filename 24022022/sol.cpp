#include "sol.h"

Sol::Sol(int x, int y,bool cible):Fixe(x,y){
    this->cible=cible;
}

Sol::Sol(const Sol &s):Fixe(s){
    cible=s.cible;
}

bool Sol::getCible() const{
    return cible;
}

void Sol::dessinerObjetGraphique(QPainter*p, Coordonnees pos, int t){
    Coordonnees c=getCoordonnees();
    bool test=getCible();
    if (test){
        p->drawPixmap(pos.y+c.y*t,pos.x+c.x*t,t,t,QPixmap(":/images/cible.jpg"));
    }
    else {
        p->drawPixmap(pos.y+c.y*t,pos.x+c.x*t,t,t,QPixmap(":/images/sol.jpg"));
    }
}

int Sol::getType() const{
    return 2;
}
