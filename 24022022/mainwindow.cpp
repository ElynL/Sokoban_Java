#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include <QScreen>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    string s = lien + "/Niveau 1.txt";
    g = new Niveau(s);
}

MainWindow::~MainWindow()
{
    delete g;
}

void MainWindow::paintEvent(QPaintEvent *e){
    QWidget::paintEvent(e);
    QPainter painter(this);
    if (g!=NULL){
        g->afficherPlateau(&painter);
        g->afficherCaisse(&painter);
        g->afficherPerso(&painter);
        painter.setFont(QFont("Arial",20));
        painter.drawText(int(800/2)-40,40,QString(QString::number(g->getNbPas())));
        painter.drawPixmap(int(800/2)-130,5,80,50,QPixmap(":/images/pas.png"));
        painter.setFont(QFont("Arial",20));
        painter.drawText(int(800/2),40,QString("Niveau "+QString::number(g->getNbNiveau())));
    }
}


void MainWindow::keyPressEvent (QKeyEvent * event ) {
    if (not g->NiveauTermine()){
        switch(event->key()){
            Coordonnees c;
            case Qt::Key_Left : c.x=0;
                             c.y=-1;
                             g->deplacerPerso(c);
                             break;
            case Qt::Key_Right : c.x=0;
                             c.y=1;
                             g->deplacerPerso(c);
                             break;
            case Qt::Key_Down : c.x=1;
                             c.y=0;
                             g->deplacerPerso(c);
                             break;
            case Qt::Key_Up : c.x=-1;
                             c.y=0;
                             g->deplacerPerso(c);
                             break;
        }
        this->repaint();
        //Fenetre d'affichage en case de victoire
        if(g->NiveauTermine()){
            if(g->getNbNiveau() != 20){
                QMessageBox msgBox;
                msgBox.setText("BRAVO POUR VOTRE VICTOIRE !!!");
                msgBox.setInformativeText("Voulez vous passer au niveau suivant ?");
                msgBox.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
                //passer au niveau suivant
                if(msgBox.exec()==QMessageBox::Yes){
                    g = g->niveauSuivant();
                    this->repaint();
                }
                //Quitter le jeu
                else{
                    QApplication::quit();
                    this->close();
                }
            }
            else{
                QMessageBox msgBox;
                msgBox.setText("BRAVO POUR VOTRE VICTOIRE !!!");
                msgBox.setInformativeText("Vous avez fini le jeu");
                msgBox.setStandardButtons(QMessageBox::Close);
                if(msgBox.exec()==QMessageBox::Close){
                    this->close();
                }
            }
        }
    }
}


void MainWindow::on_BRenitialiser_clicked()
{
    g = g->renitialiserNiveau();
    this->repaint();
}

void MainWindow:: changeNiveau(string nom){
    g = new Niveau(nom);
}

void MainWindow::on_BQuitter_clicked()
{
    this->close();
}
