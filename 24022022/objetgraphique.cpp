#include "objetgraphique.h"

ObjetGraphique::ObjetGraphique(int x,int y){
    c.x=x;
    c.y=y;
}

ObjetGraphique::ObjetGraphique(const ObjetGraphique &o){
    c.x=o.c.x;
    c.y=o.c.y;
}

Coordonnees ObjetGraphique::getCoordonnees() const{
    return c;
}

void ObjetGraphique::initialiser(Coordonnees nC){
    c.x=nC.x;
    c.y=nC.y;
}

ObjetGraphique::~ObjetGraphique(){}

