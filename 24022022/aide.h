/**
  * @file aide.h
  * @brief Contient la déclaration de la classe Aide
  * @details La classe \c Aide permet d'afficher l'aide du jeu
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/

#ifndef AIDE_H
#define AIDE_H

#include <QMainWindow>

namespace Ui {
class Aide;
}

class Aide : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Aide
     * @param parent : pointeur de QWidget
     */
    explicit Aide(QWidget *parent = nullptr);
    ~Aide();

private:
    Ui::Aide *ui;
};

#endif // AIDE_H
