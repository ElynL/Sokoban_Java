#include "niveau.h"
#include <QScreen>
#include <QApplication>

Niveau::Niveau(string file){
    string line;
    char temp;
    ifstream fichier(file.c_str(), ios::in);
    if(!fichier.fail()){
        while(fichier.eof()==false){
            fichier>>nbNiveau>>hauteur>>largeur>>nbCaisse;
            nbCibleCouverte=0;
            nbPas=0;
            taille = min(min((800-20)/largeur,(800-40-40)/hauteur),100);
            pos.y=int((800-taille*largeur)/2);
            pos.x=int((800-taille*hauteur)/2)+30;
            for(int i=0;i<hauteur;i++){
                vector<Fixe*> tempC;
                for(int j=0;j<largeur;j++){
                    fichier>>temp;
                    switch (temp) {
                        case '#' : tempC.push_back(new Mur(i,j));
                                   break;
                        case '_' : tempC.push_back(new Sol(i,j,false));
                                   break;
                        case '.' : tempC.push_back(new Sol(i,j,true));
                                   listeCible.push_back(new Sol(i,j,true));
                                   break;
                        case '*' : tempC.push_back(new Sol(i,j,true));
                                   listeCible.push_back(new Sol(i,j,true));
                                   listeCaisse.push_back(new Caisse(i,j));
                                   nbCibleCouverte++;
                                   break;
                        case '$' : tempC.push_back(new Sol(i,j,false));
                                   listeCaisse.push_back(new Caisse(i,j));
                                   break;
                        case '+' : tempC.push_back(new Sol(i,j,true));
                                   listeCible.push_back(new Sol(i,j,true));
                                   Perso = new Personnage(i,j);
                                   break;
                        case '@' : tempC.push_back(new Sol(i,j,false));
                                   Perso = new Personnage(i,j);
                                   break;
                    }
                }
                plateau.push_back(tempC);
            }
        }
    }
    else {
        cout << "Impossible d'ouvrir le fichier";
    }
    fichier.close();
}

Niveau::~Niveau(){
    delete Perso;
}

void Niveau::afficherPlateau(QPainter *p){
    for(int i=0;i<hauteur;i++){
        for(int j=0;j<largeur;j++){
           plateau[i][j]->dessinerObjetGraphique(p,pos,taille);
        }
    }
}

void Niveau::afficherPerso(QPainter *p){
    Perso->dessinerObjetGraphique(p,pos, taille);
}

bool Niveau::existeDansListeCible(Coordonnees c){
    bool test = false;
    for (int i=0;i<nbCaisse;i++){
        Coordonnees cc = listeCible[i]->getCoordonnees();
        if ((cc.x==c.x)&&(cc.y==c.y)){
            test=true;
        }
    }
    return test;
}

bool Niveau::existeDansListeCaisse(Coordonnees c){
    bool test = false;
    for (int i=0;i<nbCaisse;i++){
        Coordonnees cc = listeCaisse[i]->getCoordonnees();
        if ((cc.x==c.x)&&(cc.y==c.y)){
            test=true;
        }
    }
    return test;
}

void Niveau::afficherCaisse(QPainter *p){
    for(int i=0; i<nbCaisse;i++){
        Coordonnees c = listeCaisse[i]->getCoordonnees();
        bool test=existeDansListeCible(c);
        if (test){
            listeCaisse[i]->dessinerObjetGraphique(p,pos,taille,1);
        }
        else{
            listeCaisse[i]->dessinerObjetGraphique(p,pos,taille,0);
        }
    }
}

int Niveau::getLargeur(){
    return largeur;
}
int Niveau::getHauteur(){
    return hauteur;
}

void Niveau::pousserCaisse(Coordonnees coorC, Coordonnees d){
    Coordonnees c;
    c.x=coorC.x+d.x;
    c.y=coorC.y+d.y;
    int type=plateau[c.x][c.y]->getType();
    if ((type==2)&&(not existeDansListeCaisse(c))){
        Perso->updateCoordonnees(d);
        for(int i=0;i<nbCaisse;i++){
            c=listeCaisse[i]->getCoordonnees();
            if((c.x==coorC.x)&&(c.y==coorC.y)){
                if (existeDansListeCible(c)){
                    nbCibleCouverte--;
                }
                listeCaisse[i]->updateCoordonnees(d);
                c=listeCaisse[i]->getCoordonnees();
                if (existeDansListeCible(c)){
                    nbCibleCouverte++;
                }
            }
        }
    }
}

void Niveau::deplacerPerso(Coordonnees d){
    Coordonnees coorP=Perso->getCoordonnees();
    int type=plateau[coorP.x+d.x][coorP.y+d.y]->getType();
    if (type==2){
        nbPas++;
        Coordonnees c;
        c.x=coorP.x+d.x;
        c.y=coorP.y+d.y;
        if (existeDansListeCaisse(c)){
            pousserCaisse(c,d);
        }
        else{
            Perso->updateCoordonnees(d);
        }
    }
}

Personnage* Niveau::getPersonage(){
    return Perso;
}

bool Niveau::NiveauTermine(){
    return (nbCibleCouverte==nbCaisse);
}

Niveau* Niveau::renitialiserNiveau(){
    delete Perso;
    for (int i=0;i<nbCaisse;i++){
        delete listeCible[i];
        delete listeCaisse[i];
    }
    listeCible.clear();
    listeCaisse.clear();
    for (int i=0;i<hauteur;i++){
        for(int j=0;j<largeur;j++){
            delete plateau[i][j];
        }
        plateau[i].clear();
    }
    plateau.clear();
    string s=lien+"/Niveau "+ to_string(nbNiveau);
    s=s+".txt";
    return new Niveau(s);
}


Niveau* Niveau::niveauSuivant(){
    nbNiveau++;
    delete Perso;
    for (int i=0;i<nbCaisse;i++){
        delete listeCible[i];
        delete listeCaisse[i];
    }
    listeCible.clear();
    listeCaisse.clear();
    for (int i=0;i<hauteur;i++){
        for(int j=0;j<largeur;j++){
            delete plateau[i][j];
        }
        plateau[i].clear();
    }
    plateau.clear();
    string s=lien+"/Niveau "+ to_string(nbNiveau);
    s=s+".txt";
    return new Niveau(s);
}

int Niveau::getNbNiveau(){
    return nbNiveau;
}

int Niveau::getNbPas(){
    return nbPas;
}


