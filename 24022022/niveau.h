/**
  * @file niveau.h
  * @brief Contient la déclaration de la classe Niveau
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef Niveau_H
#define Niveau_H
#include "mur.h"
#include "sol.h"
#include "caisse.h"
#include "personnage.h"
#include <QVector>
#include <iostream>
#include <fstream>
#include <string>

class Niveau
{
private :
    int largeur;
    int hauteur;
    int nbCaisse;
    int nbCibleCouverte;
    int nbNiveau;
    int nbPas;
    int taille;
    Coordonnees pos;
    vector<vector<Fixe*>> plateau;
    vector<Caisse*> listeCaisse;
    vector<Sol*> listeCible;
    Personnage *Perso;


public:
    /**
     * @brief Niveau
     * @param file : chaine de caractère
     * @details : permet de construire un niveau
     */
    Niveau(string file);
    ~Niveau();
    /**
     * @brief deplacerPerso
     * @param d : Coordonnées
     * @details : permet de déplacer le personnage dans la direction d
     */
    void deplacerPerso(Coordonnees d);
    /**
     * @brief pousserCaisse
     * @param coorC : Coordonnées de la caisse
     * @param d : Coordonnées
     * @details : permet de pousser la caisse de coordonnées coorC dans la direction d
     */
    void pousserCaisse(Coordonnees coorC, Coordonnees d);
    /**
     * @brief getPersonage
     * @return un pointeur de personnage
     */
    Personnage* getPersonage();
    /**
     * @brief existeDansListeCaisse
     * @param c : Coordonnées
     * @details : test s'il existe une caisse de coordonnées c
     * @return : returne vrai si une telle caisse existe, faux sinon
     */
    bool existeDansListeCaisse(Coordonnees c);
    /**
     * @brief existeDansListeCible
     * @param c : Coordonnées
     * @details : test s'il existe une cible de coordonnées c
     * @return : returne vrai si une telle cible existe, faux sinon
     */
    bool existeDansListeCible(Coordonnees c);
    /**
     * @brief afficherPlateau
     * @param p : pointeur de QPainter
     * @details : permet d'afficher le plateau à l'écran
     */
    void afficherPlateau(QPainter *p);
    /**
     * @brief afficherPerso
     * @param p : pointeur de QPainter
     * @details : permet d'afficher le personnage à l'écran
     */
    void afficherPerso(QPainter *p);
    /**
     * @brief afficherCaisse
     * @param p : pointeur de QPainter
     * @details : permet d'afficher les caisses à l'écran
     */
    void afficherCaisse(QPainter *p);
    /**
     * @brief getLargeur
     * @return la largeur du plateau
     */
    int getLargeur();
    /**
     * @brief getHauteur
     * @return la hauteur du plateau
     */
    int getHauteur();
    /**
     * @brief getNbNiveau
     * @return le niveau joué
     */
    int getNbNiveau();
    /**
     * @brief NiveauTermine
     * @details : test si le jeu est fini, c'est-à-dire si toutes les cibles sont recouvertes par des cibles
     * @return vrai si la partie est fini, faux sinon
     */
    bool NiveauTermine();
    /**
     * @brief renitialiserNiveau
     * @details : permet de rénitialiser le niveau
     * @return un pointeur de niveau
     */
    Niveau* renitialiserNiveau();
    /**
     * @brief niveauSuivant
     * @details : permet de passer au niveau suivant
     * @return un pointeur de niveau
     */
    Niveau* niveauSuivant();
    /**
     * @brief getNbPas
     * @return le nombre de pas effectués
     */
    int getNbPas();
};

#endif // Niveau_H
