/**
  * @file fixe.h
  * @brief Contient la déclaration de la classe Fixe
  * @details La classe \c Fixe hérite de la classe \c Objet \c Graphique
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef FIXE_H
#define FIXE_H

#include <objetgraphique.h>
class Fixe:public ObjetGraphique{
    public:
    /**
         * @brief Fixe
         * @param x : Coordonnées en abscisses
         * @param y : Coordonnées en ordonnées
         */
        Fixe(int x=0, int y=0);
        // Méthode virtuelle : destructeur
        virtual ~Fixe();
        /**
         * @brief Fixe
         * @param f : référence de Fixe
         * @details : permet de recopier un objet Fixe
         */
        Fixe(const Fixe &f);
        // Méthode virtuelle getType
        virtual int getType() const =0;
};

#endif // FIXE_H
