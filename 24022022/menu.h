/**
  * @file menu.h
  * @brief Contient la déclaration de la fenêtre Menu
  * @details La fenêtre menu est la page d'accueil du jeu
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef MENU_H
#define MENU_H

#include <QMainWindow>
#include "mainwindow.h"
#include "aide.h"

namespace Ui {
class Menu;
}

class Menu : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Menu
     * @param a : pointeur d'Aide
     * @param m : pointeur de MainWindow
     * @param parent : pointeur de QWidget
     */
    explicit Menu(Aide *a,MainWindow *m, QWidget *parent = nullptr);
    ~Menu();

private slots:
    /**
     * @brief on_BJouer_clicked
     * @details : permet de lancer la partie après avoir cliqué sur le bouton
     */
    void on_BJouer_clicked();
    /**
     * @brief on_BQuitter_clicked
     * @details : permet de quitter le jeu après avoir cliqué sur le bouton
     */
    void on_BQuitter_clicked();
    /**
     * @brief on_BAide_clicked
     * @details : permet d'afficher l'aide après avoir cliqué sur le bouton
     */
    void on_BAide_clicked();
    /**
     * @brief on_comboBox_activated
     * @param arg1 : chaine de caractère
     * @details : permet de choisir le niveau de départ. Si aucun choix n'est fait, le jeu débute au niveau 1
     */
    void on_comboBox_activated(const QString &arg1);
    /**
     * @brief on_BFin_clicked
     * @details : permet de quitter le jeu après avoir cliqué sur le bouton
     */
    void on_BFin_clicked();

private:
    Ui::Menu *ui;
    MainWindow *niveau;
    Aide *aide;
};

#endif // MENU_H
