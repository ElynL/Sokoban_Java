#include "mur.h"

Mur::Mur(int x, int y):Fixe(x,y){
}

Mur::Mur(const Mur &m):Fixe(m){
}

void Mur::dessinerObjetGraphique(QPainter *p , Coordonnees pos, int t){
    Coordonnees c=getCoordonnees();
    p->drawPixmap(pos.y+c.y*t ,pos.x+c.x*t,t,t,QPixmap(":/images/mur.jpg"));
}

int Mur::getType() const{
    return 1;
}
