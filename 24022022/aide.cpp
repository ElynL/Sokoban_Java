#include "aide.h"
#include "ui_aide.h"

Aide::Aide(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Aide)
{
    ui->setupUi(this);
    QPixmap bkgnd(":/images/aide.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
    this->setWindowState(Qt::WindowMaximized);
}

Aide::~Aide()
{
    delete ui;
}
