/**
  * @file personnage.h
  * @brief Contient la déclaration de la classe Personnage
  * @details La classe \c Personnage hérite de la classe \c Mobile
  * @author Léa Houdiard et Elyn Lemonnier
  * @date Novembre 2021 / Mars 2022
*/


#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include "mobile.h"

class Personnage: public Mobile
{
public:
    /**
     * @brief Personnage
     * @param x : coordonnées en abscisse du personnage
     * @param y : coordonnées en ordonnées du personnage
     * @details : initialise les coordonnées du personnage à la position (x,y)
     */
    Personnage(int x=0, int y=0);
    /**
     * @brief Personnage
     * @param x : coordonnées en abscisse du personnage
     * @param y : coordonnées en ordonnées du personnage
     * @details : initialise les coordonnées du personnage à la position (x,y)
     */
    Personnage(const Personnage &p);
    /**
     * @brief dessinerObjetGraphique
     * @param p : pointeur de QPainter
     * @param pos : Coordonnées du point en haut à gauche du plateau
     * @param t : entier correspondant à la taille en pixels du personnage à l'écran
     * @details : permet l'affichage du personnage à l'écran
     */
    void dessinerObjetGraphique(QPainter*p,Coordonnees pos, int t);
};

#endif // PERSONNAGE_H
