package code;


abstract class Objet_Graphique {
	
	class Coordonnees { public int x=0, y=0; }
	protected Coordonnees c;
	
	final String lien="/Polytech/ellemonnie/24022022";
	   
	public Objet_Graphique() {	
	}
	
	public Objet_Graphique(int x, int y) {
		this.c.x = x;
		this.c.y = y;
	}
	
	public Objet_Graphique(Objet_Graphique o) {
		c.x = o.c.x;
		c.y = o.c.y;
	}
	        
	public Coordonnees getCoordonnees() {
		return c;
	}
	        
	public void initialiser(Coordonnees nC) {
		c.x = nC.x;
		c.y = nC.y;
	}
	
	//public abstract void dessinerObjetGraphique(QPainter *p, Coordonnees pos, int t);

}
