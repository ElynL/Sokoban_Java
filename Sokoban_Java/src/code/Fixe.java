package code;

abstract class Fixe extends Objet_Graphique {

	public Fixe() {
	}
	
	public Fixe(int x, int y) {
		super(x,y);
	}
	      
	public Fixe(Fixe f) {
		super(f);
	}
	// Méthode virtuelle getType
	public abstract int getType();
}
